package company

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

class DatabaseProfiles {

	static String profilesFilePath = 'Include/config/dbProfiles.json'
	static String urlTemplatesFilePath = 'Include/config/url.json'

	static Map<String, Object> loadProfiles() {
		return loadJson(profilesFilePath)
	}

	static void saveProfiles(Map<String, Object> profiles) {
		saveJson(profilesFilePath, profiles)
	}

	static Map<String, Object> loadUrlTemplates() {
		return loadJson(urlTemplatesFilePath)
	}

	private static Map<String, Object> loadJson(String filePath) {
		File configFile = new File(filePath)
		if (!configFile.exists()) {
			configFile.parentFile.mkdirs()
			configFile.text = '{}'
		}
		JsonSlurper jsonSlurper = new JsonSlurper()
		return jsonSlurper.parse(configFile)
	}

	private static void saveJson(String filePath, Map<String, Object> data) {
		File configFile = new File(filePath)
		configFile.text = JsonOutput.prettyPrint(JsonOutput.toJson(data))
	}
}
