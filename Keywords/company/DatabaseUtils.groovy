package company

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.sql.Sql
import groovy.sql.GroovyRowResult
import internal.GlobalVariable
import java.sql.CallableStatement
import java.sql.ResultSet
import com.kms.katalon.core.util.KeywordUtil
public class DatabaseUtils {

	@Keyword
	public static List<GroovyRowResult> getQueryResult(String dbProfile, String query) {
		def profiles = DatabaseProfiles.loadProfiles()
		def urlTemplates = DatabaseProfiles.loadUrlTemplates()
		def profile=profiles[dbProfile]
		if (profile == null) {
			throw new IllegalArgumentException("No database profile found for: ${dbProfile}")
		}
		String url = urlTemplates[profile["databaseEngine"]]
		url = url.replace('{host}', profile['host'])
				.replace('{port}', profile['port'].toString())
				.replace('{databaseName}', profile['databaseName'])

		def sql = Sql.newInstance(url,profile["username"],profile["password"], profile["databaseDriver"])
		def List<GroovyRowResult>  results = []
		try {
			//			sql.eachRow(query) { GroovyRowResult row ->
			//				// Process each row
			//				results.add(row.toRowResult())
			//			}
			results= sql.rows(query)
		}
		//		catch (Exception e){
		//			results.add(new GroovyRowResult(["message":e.message]))
		//		}
		finally {
			// Close the SQL connection
			sql.close()
		}
		return results
	}

	@Keyword
	public static List<Map<String, Object>> executeStoredProcedure(String databaseProfile, String SpName, String parametersString) {
		// Load database profiles and URL templates
		def profiles = DatabaseProfiles.loadProfiles()
		def urlTemplates = DatabaseProfiles.loadUrlTemplates()
		List<Map<String, Object>> result=[]
		// Get the profile and URL template
		def dbConfig = profiles[databaseProfile]
		if (dbConfig == null) {
			throw new IllegalArgumentException("No database profile found for: ${databaseProfile}")
		}

		String databaseEngine = dbConfig['databaseEngine']
		String urlTemplate = urlTemplates[databaseEngine]
		if (!urlTemplate) {
			throw new IllegalArgumentException("No URL template found for database engine: ${databaseEngine}")
		}

		// Build the URL
		String url = urlTemplate.replace('{host}', dbConfig['host'])
				.replace('{port}', dbConfig['port'].toString())
				.replace('{databaseName}', dbConfig['databaseName'])
		def sql = Sql.newInstance(url, dbConfig['username'], dbConfig['password'], dbConfig['databaseDriver'])
				
		// Parse the parameters string
		def params = parseParameters(parametersString)
		def Map<Integer, Object> inputParams = params.inputParams
		def Map<Integer, Integer> outputParams = params.outputParams
		try {
			// Build stored procedure call statement
			String storedProcedure = "{call ${SpName}(${generatePlaceholders(inputParams.size() + outputParams.size())})}"
		
			// Prepare the callable statement to execute the stored procedure
			// Prepare the callable statement to execute the stored procedure
			sql.call(storedProcedure) { CallableStatement callableStatement ->
                	// Set input parameters
                	inputParams.each { index, value ->
						KeywordUtil.logInfo("input parameter")
						callableStatement.setObject(index, value)
					}

					// Register output parameters dynamically
					outputParams.each { index, sqlType ->
						KeywordUtil.logInfo("output parameter")
						callableStatement.registerOutParameter(index, sqlType)
					}

					// Execute the stored procedure
					boolean hasResultSet = callableStatement.execute()

					// Retrieve and print output parameter values
					def outputValues = [:]
					
					outputParams.each { index, sqlType ->
						def outputValue = callableStatement.getObject(index)
						outputValues.put("OUT${index}", outputValue)
					}

					if (!outputValues.isEmpty()) {
						result << outputValues
					} else {
						// Handle the result set if no output parameters are available
						def resultSetStatus = [:]
						if (hasResultSet) {
							def resultSet = callableStatement.getResultSet()
							while (resultSet.next()) {
								resultSetStatus.put("STATUS", resultSet.getString(1)) // Example of accessing the first column
							}
						}
						if (!resultSetStatus.isEmpty()) {
							result << resultSetStatus
						} else {
							result << ["STATUS": "Stored Procedure executed successfully without result set"]
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace()
				result << ["ERROR": e.message]
			} finally {
				// Clean up and close resources
				try {
					if (sql != null) {
						sql.close()
					}
				} catch (Exception e) {
					e.printStackTrace()
				}
			}
	
			return result
	}

	private static Map<String, Map<Integer, ?>> parseParameters(String paramsString) {
        Map<Integer, Object> inputParams = new LinkedHashMap<>()
        Map<Integer, Integer> outputParams = new LinkedHashMap<>()
        int paramIndex = 1
        
        paramsString.split(',').each { param ->
            param = param.trim()
            if (param.startsWith("IN:")) {
                inputParams.put(paramIndex, param.substring(3).trim())
            } else if (param.startsWith("OUT:")) {
                String sqlType = param.substring(4).trim().toUpperCase()
                int typeCode = getTypeCode(sqlType)
                outputParams.put(paramIndex, typeCode)
            }
            paramIndex++
        }
        
        return [inputParams : inputParams,outputParams: outputParams]
    }

	
	private static String generatePlaceholders(int count) {
		return (1..count).collect { "?" }.join(", ")
	}

	private static int getTypeCode(String sqlType) {
		switch (sqlType) {
			case "INTEGER": return java.sql.Types.INTEGER
			case "VARCHAR": return java.sql.Types.VARCHAR
			case "DOUBLE": return java.sql.Types.DOUBLE
			case "BOOLEAN": return java.sql.Types.BOOLEAN
			// Add more SQL types as needed
			default: throw new IllegalArgumentException("Unsupported SQL type: " + sqlType)
		}
	}
	
}
