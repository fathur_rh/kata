import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import static company.DatabaseUtils.executeStoredProcedure
import static company.DatabaseUtils.parseParameters
//String dbProfile=dbProfile
//String spName=SpName
//String parameter=Parameter

String dbProfile = 'databaseKpa'
String spName = 'CHECK_AGGR_NO_404'
String parameter = 'IN:"EDV",IN:"110102050220230010",OUT:VARCHAR,OUT:INTEGER'
//def params = parseParameters(parameter)
//def Map<Integer, Object> inputParams = params.inputParams
//def Map<Integer, Integer> outputParams = params.outputParams
//println inputParams
//println outputParams
List<Map<String, Object>> results = executeStoredProcedure(dbProfile,spName,parameter)

println "Stored Procedure Results: ${results}"